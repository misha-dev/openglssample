package com.example.openglsample.shapes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square {

	private FloatBuffer vertexBuffer;
	private ShortBuffer drawListBuffer;
	
	static final int SIZE_OF_FLOAT = 4;
	static final int SIZE_OF_SHORT = 2;
	static final int COORDS_PER_VERTEX = 3;
	static float[] sqaureCoords = {
        -0.5f,  0.5f, 0.0f,   // top left
        -0.5f, -0.5f, 0.0f,   // bottom left
         0.5f, -0.5f, 0.0f,   // bottom right
         0.5f,  0.5f, 0.0f 	 // top right
	};
	
	private short[] drawOrder = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices
	
	public Square() {
		
		ByteBuffer bb = ByteBuffer.allocateDirect(sqaureCoords.length * SIZE_OF_FLOAT);
		
		bb.order(ByteOrder.nativeOrder());
		
		vertexBuffer = bb.asFloatBuffer();
		vertexBuffer.put(sqaureCoords);
		vertexBuffer.position(0);
		
        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(drawOrder.length * SIZE_OF_SHORT);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);
		
	}
	
}

