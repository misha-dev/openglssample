package com.example.openglsample;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MotionEvent;

import com.example.openglsample.shapes.Square;
import com.example.openglsample.shapes.Triangle;

public class OglActivity extends Activity {

	 private GLSurfaceView mGLView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mGLView = new MyGLSurfaceView(this);
		setContentView(mGLView);
	}
	

	
	static public class MyGLRenderer implements GLSurfaceView.Renderer {

		public volatile float mAngle;
		
		private Triangle mTriangle;
		private Square mSquare;

	    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
	    private final float[] mMVPMatrix = new float[16];
	    private final float[] mProjectionMatrix = new float[16];
	    private final float[] mViewMatrix = new float[16];
	    private final float[] mRotationMatrix = new float[16];
		
		@Override
	    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
	        // Set the background frame color
	        GLES20.glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
	        
	        mTriangle = new Triangle();
	        mSquare = new Square();
	    }

	    public void onDrawFrame(GL10 unused) {
	        // Redraw background color
	        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	        
	        // Set the camera position (View matrix)
	        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

	        // Calculate the projection and view transformation
	        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

	        float[] scratch = new float[16];
//	        long time = SystemClock.uptimeMillis() % 4000L;
//	        float angle = 0.090f * ((int)time);
	        
	        Matrix.setRotateM(mRotationMatrix, 0, mAngle, 0, 0, -1.0f);
	        
	        Matrix.multiplyMM(scratch,  0, mMVPMatrix, 0, mRotationMatrix, 0);
	        
	        // Draw shape
	        mTriangle.draw(scratch);
	        
	    }

	    public void onSurfaceChanged(GL10 unused, int width, int height) {
	        GLES20.glViewport(0, 0, width, height);
	        
	        float ratio = (float) width / height;

	        // this projection matrix is applied to object coordinates
	        // in the onDrawFrame() method
	        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 2, 7);
	    }

		public float getAngle() {
			return mAngle;
		}

		public void setAngle(float angle) {
			mAngle = angle;
		}

	}
	
	
	
	class MyGLSurfaceView extends GLSurfaceView {

	    private static final float TOUCH_SCALE_FACTOR = -1;
		private float mPreviousX;
		private float mPreviousY;
		private MyGLRenderer mRenderer;

		public MyGLSurfaceView(Context context){
	        super(context);
	        
	        setEGLContextClientVersion(2);
	        
	        setRenderer(mRenderer = new MyGLRenderer());
	        
	        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	    }
	    
	    @Override
	    public boolean onTouchEvent(MotionEvent e) {
	        // MotionEvent reports input details from the touch screen
	        // and other input controls. In this case, you are only
	        // interested in events where the touch position changed.

	        float x = e.getX();
	        float y = e.getY();

	        switch (e.getAction()) {
	            case MotionEvent.ACTION_MOVE:

	                float dx = x - mPreviousX;
	                float dy = y - mPreviousY;

	                // reverse direction of rotation above the mid-line
	                if (y > getHeight() / 2) {
	                  dx = dx * -1 ;
	                }

	                // reverse direction of rotation to left of the mid-line
	                if (x < getWidth() / 2) {
	                  dy = dy * -1 ;
	                }

	                mRenderer.setAngle(
	                        mRenderer.getAngle() +
	                        ((dx + dy) * TOUCH_SCALE_FACTOR));  // = 180.0f / 320
	                requestRender();
	        }

	        mPreviousX = x;
	        mPreviousY = y;
	        return true;
	    }
	}

}
